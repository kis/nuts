/**
 * Copyright (c) 2018 人人开源 All rights reserved.
 *
 * 
 *
 * 版权所有，侵权必究！
 */

package com.nuts.dao;

import org.apache.ibatis.annotations.Mapper;

/**
 * SQLServer代码生成器
 *
 * @author Mark sunlightcs@gmail.com
 */
@Mapper
public interface SQLServerGeneratorDao extends GeneratorDao {

}
