/**
 * Copyright (c) 2018 人人开源 All rights reserved.
 *
 * 
 *
 * 版权所有，侵权必究！
 */

package com.nuts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * -generator
 *
 * @author Mark sunlightcs@gmail.com
 */
@SpringBootApplication
public class GeneratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(GeneratorApplication.class, args);
	}
}
