# nuts

#### 介绍
- nuts是一个轻量级的，前后端分离的Java快速开发平台，能快速开发项目【接私活利器】


<br>

#### 软件架构

- 采用SpringBoot、Shiro、MyBatis-Plus、Vue3、Element Plus框架，开发的一套权限系统，极低门槛，拿来即用。
- 提供了代码生成器，只需编写30%左右代码，其余的代码交给系统自动生成，可快速完成开发任务
- 文档采用smart-doc无入侵方式，生成后可以使用apifox进行调试，有条件可以使用torna （在开发中）
- 本系统是基于人人系统进行二开

#### 前端页面 （在开发中）

- 后台技术：Vue3、Element Plus等框架
- 后台演示：xxxxxx
- 移动端技术：使用uni-app，对多端进行支持
- 移动端演示：xxxxx

#### 安装教程

1.  环境jdk17、nodejs 20 相关环境自己安装
2.  开发工具采用idea、 HBuilder X、
3.  数据库mysql8、redis6

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


