/**
 * Copyright (c) 2018 人人开源 All rights reserved.
 *
 *
 *
 * 版权所有，侵权必究！
 */

package com.nuts.modules.security.dto;

//import io.swagger.annotations.ApiModel;
//import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 登录表单
 *
 * @author Mark sunlightcs@gmail.com
 */
@Data
//@(value = "登录表单")
public class LoginDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    //@Property(value = "用户名", required = true)
    @NotBlank(message="{sysuser.username.require}")
    private String username;

    //@Property(value = "密码")
    @NotBlank(message="{sysuser.password.require}")
    private String password;

    //@Property(value = "验证码")
    @NotBlank(message="{sysuser.captcha.require}")
    private String captcha;

    //@Property(value = "唯一标识")
    @NotBlank(message="{sysuser.uuid.require}")
    private String uuid;

}