/**
 * Copyright (c) 2018 人人开源 All rights reserved.
 *
 *
 *
 * 版权所有，侵权必究！
 */

package com.nuts.modules.oss.service;

import com.nuts.common.page.PageData;
import com.nuts.common.service.BaseService;
import com.nuts.modules.oss.entity.SysOssEntity;

import java.util.Map;

/**
 * 文件上传
 * 
 * @author Mark sunlightcs@gmail.com
 */
public interface SysOssService extends BaseService<SysOssEntity> {

	PageData<SysOssEntity> page(Map<String, Object> params);
}
