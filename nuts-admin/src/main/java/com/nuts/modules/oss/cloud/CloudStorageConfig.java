/**
 * Copyright (c) 2018 人人开源 All rights reserved.
 * <p>
 *
 * <p>
 * 版权所有，侵权必究！
 */

package com.nuts.modules.oss.cloud;

import com.nuts.common.validator.group.AliyunGroup;
import com.nuts.common.validator.group.QcloudGroup;
import com.nuts.common.validator.group.QiniuGroup;
//import io.swagger.annotations.ApiModel;
//import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Range;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 云存储配置信息
 *
 * @author Mark sunlightcs@gmail.com
 */
@Data
//@(value = "云存储配置信息")
public class CloudStorageConfig implements Serializable {
    private static final long serialVersionUID = 1L;

    //@Property(value = "类型 1：七牛  2：阿里云  3：腾讯云 ")
    @Range(min = 1, max = 6, message = "{oss.type.range}")
    private Integer type;

    //@Property(value = "七牛绑定的域名")
    @NotBlank(message = "{qiniu.domain.require}", groups = QiniuGroup.class)
    @URL(message = "{qiniu.domain.url}", groups = QiniuGroup.class)
    private String qiniuDomain;

    //@Property(value = "七牛路径前缀")
    private String qiniuPrefix;

    //@Property(value = "七牛ACCESS_KEY")
    @NotBlank(message = "{qiniu.accesskey.require}", groups = QiniuGroup.class)
    private String qiniuAccessKey;

    //@Property(value = "七牛SECRET_KEY")
    @NotBlank(message = "{qiniu.secretkey.require}", groups = QiniuGroup.class)
    private String qiniuSecretKey;

    //@Property(value = "七牛存储空间名")
    @NotBlank(message = "{qiniu.bucketname.require}", groups = QiniuGroup.class)
    private String qiniuBucketName;

    //@Property(value = "阿里云绑定的域名")
    @NotBlank(message = "{aliyun.domain.require}", groups = AliyunGroup.class)
    @URL(message = "{aliyun.domain.url}", groups = AliyunGroup.class)
    private String aliyunDomain;

    //@Property(value = "阿里云路径前缀")
    private String aliyunPrefix;

    //@Property(value = "阿里云EndPoint")
    @NotBlank(message = "{aliyun.endPoint.require}", groups = AliyunGroup.class)
    private String aliyunEndPoint;

    //@Property(value = "阿里云AccessKeyId")
    @NotBlank(message = "{aliyun.accesskeyid.require}", groups = AliyunGroup.class)
    private String aliyunAccessKeyId;

    //@Property(value = "阿里云AccessKeySecret")
    @NotBlank(message = "{aliyun.accesskeysecret.require}", groups = AliyunGroup.class)
    private String aliyunAccessKeySecret;

    //@Property(value = "阿里云BucketName")
    @NotBlank(message = "{aliyun.bucketname.require}", groups = AliyunGroup.class)
    private String aliyunBucketName;

    //@Property(value = "腾讯云绑定的域名")
    @NotBlank(message = "{qcloud.domain.require}", groups = QcloudGroup.class)
    @URL(message = "{qcloud.domain.url}", groups = QcloudGroup.class)
    private String qcloudDomain;

    //@Property(value = "腾讯云路径前缀")
    private String qcloudPrefix;

    //@Property(value = "腾讯云AppId")
    @NotNull(message = "{qcloud.appid.require}", groups = QcloudGroup.class)
    private Integer qcloudAppId;

    //@Property(value = "腾讯云SecretId")
    @NotBlank(message = "{qcloud.secretId.require}", groups = QcloudGroup.class)
    private String qcloudSecretId;

    //@Property(value = "腾讯云SecretKey")
    @NotBlank(message = "{qcloud.secretkey.require}", groups = QcloudGroup.class)
    private String qcloudSecretKey;

    //@Property(value = "腾讯云BucketName")
    @NotBlank(message = "{qcloud.bucketname.require}", groups = QcloudGroup.class)
    private String qcloudBucketName;

    //@Property(value = "腾讯云COS所属地区")
    @NotBlank(message = "{qcloud.region.require}", groups = QcloudGroup.class)
    private String qcloudRegion;

}