/**
 * Copyright (c) 2018 人人开源 All rights reserved.
 *
 *
 *
 * 版权所有，侵权必究！
 */

package com.nuts.modules.oss.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.nuts.common.constant.Constant;
import com.nuts.common.page.PageData;
import com.nuts.common.service.impl.BaseServiceImpl;
import com.nuts.modules.oss.dao.SysOssDao;
import com.nuts.modules.oss.entity.SysOssEntity;
import com.nuts.modules.oss.service.SysOssService;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service
public class SysOssServiceImpl extends BaseServiceImpl<SysOssDao, SysOssEntity> implements SysOssService {

	@Override
	public PageData<SysOssEntity> page(Map<String, Object> params) {
		IPage<SysOssEntity> page = baseDao.selectPage(
			getPage(params, Constant.CREATE_DATE, false),
			new QueryWrapper<>()
		);
		return getPageData(page, SysOssEntity.class);
	}
}
