/**
 * Copyright (c) 2018 人人开源 All rights reserved.
 *
 * 
 *
 * 版权所有，侵权必究！
 */

package com.nuts.modules.job.dao;

import com.nuts.common.dao.BaseDao;
import com.nuts.modules.job.entity.ScheduleJobEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

/**
 * 定时任务
 *
 * @author Mark sunlightcs@gmail.com
 */
@Mapper
public interface ScheduleJobDao extends BaseDao<ScheduleJobEntity> {
	
	/**
	 * 批量更新状态
	 */
	int updateBatch(Map<String, Object> map);
}
