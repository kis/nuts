/**
 * Copyright (c) 2018 人人开源 All rights reserved.
 *
 *
 *
 * 版权所有，侵权必究！
 */

package com.nuts.modules.job.dto;

//import io.swagger.annotations.ApiModel;
//import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 定时任务日志
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0
 */
@Data
//@(value = "定时任务日志")
public class ScheduleJobLogDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    //@Property(value = "id")
    private Long id;

    //@Property(value = "任务id")
    private Long jobId;

    //@Property(value = "spring bean名称")
    private String beanName;

    //@Property(value = "参数")
    private String params;

    //@Property(value = "任务状态    0：失败    1：成功")
    private Integer status;

    //@Property(value = "失败信息")
    private String error;

    //@Property(value = "耗时(单位：毫秒)")
    private Integer times;

    //@Property(value = "创建时间")
    private Date createDate;

}
